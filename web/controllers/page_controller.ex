defmodule AnimasCodeLabs.PageController do
  use AnimasCodeLabs.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end

  def contact(conn, _parmas) do
    render conn, "contact.html"
  end
end
