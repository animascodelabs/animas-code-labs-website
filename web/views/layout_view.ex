defmodule AnimasCodeLabs.LayoutView do
  use AnimasCodeLabs.Web, :view

  def current_year do
    "2016"
  end
end
