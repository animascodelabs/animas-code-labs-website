import $ from 'jquery';

$(function(){
  $(".mobile-menu").click(function(){
    $(this).toggleClass("menu-is-active");
    $(".site-header-nav").toggleClass("active");
  });
});
