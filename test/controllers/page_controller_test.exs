defmodule AnimasCodeLabs.PageControllerTest do
  use AnimasCodeLabs.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "page home"
  end

  test "GET /contact", %{conn: conn} do
    conn = get conn, "/contact"
    assert html_response(conn, 200)
  end
end
