# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :animas_code_labs,
  ecto_repos: [AnimasCodeLabs.Repo]

# Configures the endpoint
config :animas_code_labs, AnimasCodeLabs.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "i5kIaYfeYqyl7/NX1PcrY2eUb0aHKowA8ZnFxn2k7RVSNsO9fU1zSRsS3WVQgd6b",
  render_errors: [view: AnimasCodeLabs.ErrorView, accepts: ~w(html json)],
  pubsub: [name: AnimasCodeLabs.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configure your database
config :animas_code_labs, AnimasCodeLabs.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  port: System.get_env("POSTGRES_PORT") || 5432

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
